
#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
    ofBackground(0); // Black background
    ofSetFrameRate(120);
    
    // MIDI Setup
     midiIn.openPort(0); // Open the first MIDI port.
     midiIn.addListener(this); // Register this ofApp instance as a listener to MIDI messages.
    
    // Initialize mapped variables
   //ex:  redValue = 0;
    // Initialize Strobe Effect Variables
    strobeState = false;
    strobeSpeed = 100; // Set a default strobe speed (milliseconds)
    lastStrobeTime = 0;
    backgroundColor = ofColor(0, 0, 0); // Start with a black background

    // Initialize Multi-Colored Strobe Variables
    strobeColors[0] = ofColor(255, 0, 0); // Red Strobe
    strobeColors[1] = ofColor(0, 0, 255); // Blue Strobe
    strobeColors[2] = ofColor(255,255,255);// White Strobe
   
    // For the alternating red/blue strobe, we'll handle this logic in the update/draw methods.
    currentStrobeColor = 0; // Start with the first color
    multiStrobeState = false; // Multi-color strobe is initially off
   

   
   // Preset Loading
   // Check if "preset1.json" exists in the "Presets" folder.
   
   // Load or initialize preset1
   if (ofFile::doesFileExist(ofToDataPath("Presets/preset1.json"))) {
       // Check if the preset1.json file exists in the Presets folder.
       // ofToDataPath ensures that the path is correctly constructed relative to the application's data folder.
       
       // Load preset1
       ofJson preset = ofLoadJson("Presets/preset1.json"); // Load the JSON file into an ofJson object.
       // Extract RGB values from the JSON object and assign them to the respective variables.
       // This sets the initial color of the triangle to the saved values in preset1.
       midiLoSpeed = preset["low_speed"];
       midiHiSpeed = preset["high_speed"];

       midiLoNoise = preset["low_noise"];
       midiHiNoise = preset["high_noise"];
       
       midiLoFreq = preset["low_frequency"];
       midiHiFreq = preset["high_frequency"];

       midiAmp = preset["amplitude"];
       
       // Mark preset1 as having been saved.
       // This information is used in the handlePresetPad function to decide whether to save or load this preset.
       presetSaved["preset1"] = true;
   } else {
       // If preset1.json does not exist, initialize the RGB values to 0 (black color).
       // This is the default starting state if no saved presets are found.
       midiLoSpeed = midiHiSpeed = midiLoFreq = midiHiFreq = midiLoNoise = midiHiNoise = midiAmp = 0;
       
       // Mark preset1 as not saved, indicating that it can be written to.
       // This allows users to save a new preset1 when they modify and save the color settings for the first time.
       presetSaved["preset1"] = false;
   }
   
   // Load or initialize preset2
   if (ofFile::doesFileExist(ofToDataPath("Presets/preset2.json"))) {
       // Similar logic as preset1 for checking the existence and loading of preset2.
       // If the file exists, it means the preset was previously saved and should be loaded.
       ofJson preset = ofLoadJson("Presets/preset2.json"); // Load preset2
       // The RGB values for preset2 are assumed to be loaded similarly.
       // Update the presetSaved map to reflect that preset2 has been saved.
       presetSaved["preset2"] = true;
   } else {
       // If preset2.json does not exist, mark preset2 as not saved.
       // This allows for saving a new preset2 in the future.
       presetSaved["preset2"] = false;
   }
   
   // Load or initialize preset3
   if (ofFile::doesFileExist(ofToDataPath("Presets/preset3.json"))) {
       // Check and load preset3 similarly to preset1 and preset2.
       ofJson preset = ofLoadJson("Presets/preset3.json"); // Load preset3
       // RGB values for preset3 are assumed to be loaded similarly.
       // Mark preset3 as saved in the presetSaved map.
       presetSaved["preset3"] = true;
   } else {
       // If preset3.json does not exist, mark preset3 as not saved.
       // This state enables future saving of a new preset3.
       presetSaved["preset3"] = false;
   }


    
    soundPlayer.load("03.mp3"); // Replace with your audio file path
      soundPlayer.setLoop(true);
      soundPlayer.play();

      nBandsToGet = 2048; // This is an example; you can change the number of bands
      fftSmoothed = new float[nBandsToGet];
      for (int i = 0; i < nBandsToGet; i++) {
          fftSmoothed[i] = 0;
      }

    // Dimensions of the centered square
       int squareWidth = 1080;
       int squareHeight = 1920;
    
    // Calculate the top-left corner of the square
       float startX = (ofGetWidth() - squareWidth) / 2;
       float startY = (ofGetHeight() - squareHeight) / 2;
    
    
    
    // Initialize particles and attractors
    for (int i = 0; i < 9500; i++) {
           float x = ofRandom(startX, startX + squareWidth);
           float y = ofRandom(startY, startY + squareHeight);
           Particle p(ofVec2f(x, y));
            p.setSpeed(0.01f);
            p.setSize(ofRandom(1.5, 3.5)); // Randomize the size between 0.5 and 1.5
            p.setTrailLength(trailLength); // Use the static trail length from ofApp
            particles.push_back(p);
        }

    attractors.push_back(ofVec2f(1100/2, 100));
    attractors.push_back(ofVec2f(1900/2, 100));
    attractors.push_back(ofVec2f(800, 900));
    attractors.push_back(ofVec2f(2500/2, 1300/2));

    int totalVboPoints = particles.size() * trailLength;

    // Resize VBO data storage for particles and their trails
    particlePositions.resize(totalVboPoints);
    particleColors.resize(totalVboPoints);

    // Initialize VBO with expanded size
    particleVbo.setVertexData(&particlePositions[0], totalVboPoints, GL_DYNAMIC_DRAW);
    particleVbo.setColorData(&particleColors[0], totalVboPoints, GL_DYNAMIC_DRAW);
}


//--------------------------------------------------------------
void ofApp::update() {
    
    // Update the single-color strobe effect
    if (strobeState) {
        // Check if enough time has passed based on strobeSpeed
        if (ofGetElapsedTimeMillis() - lastStrobeTime > strobeSpeed) {
            // Toggle background color between black and the current strobe color
            backgroundColor = (backgroundColor == ofColor(0, 0, 0)) ? strobeColors[currentStrobeColor] : ofColor(0, 0, 0);

            // Reset the timer
            lastStrobeTime = ofGetElapsedTimeMillis();
        }
    }

    // Update the alternating Red/Blue strobe effect
    if (altStrobeState) {
        updateAltStrobe();
    }

    
    // Perform FFT analysis for audio
    ofSoundUpdate();
    float *value = ofSoundGetSpectrum(nBandsToGet); // Get FFT spectrum data
    for (int i = 0; i < nBandsToGet; i++) {
        fftSmoothed[i] *= 0.96f;
        if (fftSmoothed[i] < value[i]) fftSmoothed[i] = value[i];
    }

    // Analyze low frequencies (20Hz to 80Hz)
    lowFreqAmplitude = 0;
    int lowBandStart = 0; // Estimate, adjust based on your audio's characteristics
    int lowBandEnd = midiLoFreq;   // Estimate, adjust based on your audio's characteristics
    for (int i = lowBandStart; i <= lowBandEnd; i++) {
        lowFreqAmplitude += fftSmoothed[i];
    }
    lowFreqAmplitude /= (lowBandEnd - lowBandStart + 1);

    // Map the low-frequency amplitude to a speed range (0.1 to 75.0)
    float mappedSpeed = ofMap(lowFreqAmplitude, 0, 1, midiLoSpeed, midiHiSpeed, true);

    // Map the low-frequency amplitude to a noise range (10 to 3000)
    float noiseMultiplier = ofMap(lowFreqAmplitude, 0, 1, midiLoNoise, midiHiNoise, true);

    // Update attractor positions with a slight random movement
    for (ofVec2f &attractor : attractors) {
        attractor.x += ofRandom(-1, 1); // Randomly move left or right
        attractor.y += ofRandom(-1, 1); // Randomly move up or down

        // Keep attractors within the window bounds
        attractor.x = ofClamp(attractor.x, 0, ofGetWidth());
        attractor.y = ofClamp(attractor.y, 0, ofGetHeight());
    }
    
    // Update particles
    for (int i = 0; i < particles.size(); ++i) {
        particles[i].setSpeed(mappedSpeed); // Adjust speed based on audio
        particles[i].update(attractors, noiseMultiplier); // Pass noiseMultiplier to particle update

        // Update particle trail positions and colors
        for (int t = 0; t < trailLength; t++) {
            int index = i * trailLength + t;

            if (t < particles[i].getTrailPositions().size()) {
                particlePositions[index] = particles[i].getTrailPositions()[t];
                float alpha = ofMap(t, 0, trailLength, 1.0, 0.0);

                if (t == 0) {
                    particleColors[index] = ofFloatColor(ofRandom(55) / 255.0f, 55 / 55.0f, 0.1f, 1.0f);
                } else if (particleColors[index].a == 0) {
                    particleColors[index] = ofFloatColor(ofRandom(255) / 255.0f, 55 / 255.0f, 1.0f, alpha);
                } else {
                    particleColors[index].a = alpha;
                }
            } else {
                particlePositions[index] = ofVec3f(0, 0, 0);
                particleColors[index] = ofFloatColor(0, 0, 0, 0);
            }
        }
    }

    // Update VBO with new particle data
    particleVbo.updateVertexData(&particlePositions[0], particlePositions.size());
    particleVbo.updateColorData(&particleColors[0], particleColors.size());
}



//--------------------------------------------------------------
void ofApp::updateAltStrobe() {
    // Check if enough time has passed based on strobeSpeed
    if (ofGetElapsedTimeMillis() - altStrobeLastTime > strobeSpeed) {
        // Cycle through Red, Black, Blue, Black, etc.
        if (altStrobeIndex == 0) {
            backgroundColor = ofColor(255, 0, 0); // Red
        } else if (altStrobeIndex == 1) {
            backgroundColor = ofColor(0, 0, 0); // Black
        } else if (altStrobeIndex == 2) {
            backgroundColor = ofColor(0, 0, 255); // Blue
        }

        // Move to the next color in the sequence
        altStrobeIndex = (altStrobeIndex + 1) % 3;

        // Reset the timer
        altStrobeLastTime = ofGetElapsedTimeMillis();
    }
}
//--------------------------------------------------------------

void ofApp::draw() {
    ofBackground(backgroundColor); // Use the backgroundColor variable for the strobe effect

    ofEnableAlphaBlending();

    for (int i = 0; i < particles.size(); ++i) {
        // Set the size for each particle
        glPointSize(particles[i].getSize() * ofGetHeight() / 1920.0f); // Adjust size relative to screen height

        // Draw each particle's trail individually
        particleVbo.draw(GL_POINTS, i * trailLength, trailLength);
    }

    ofDisableAlphaBlending();

    // Draw FPS counter
    ofSetColor(255);
    ofDrawBitmapString("FPS: " + ofToString(ofGetFrameRate()), 20, 20);
}

//--------------------------------------------------------------
void ofApp::handlePresetPad(int padControl) {
    // Generate the preset name based on the MIDI pad control number.
    // The padControl values (73, 74, 75) correspond to "preset1", "preset2", "preset3", respectively.
    // The number 72 is subtracted from padControl to align the MIDI control numbers with the preset numbering.
    std::string presetName = "preset" + std::to_string(padControl - 72);

    // Check if the preset associated with the pressed pad has already been saved.
    if (presetSaved[presetName]) {
        // If the preset has been saved previously, load it.
        // This means retrieving the RGB values stored in the preset and applying them to the current application state.
        loadPreset(presetName);
        // The loadPreset function updates the redValue, greenValue, and blueValue variables,
        // which in turn changes the color of the triangle drawn in the draw() method.
    } else {
        // If the preset has not been saved yet, save the current state as a new preset.
        // This involves storing the current RGB values into a JSON object and writing it to a file.
        savePreset(presetName);
        // The savePreset function creates or updates a JSON file named after the preset,
        // effectively saving the current color configuration for future use.
        // After this operation, the preset will be available for loading the next time the same pad is pressed.
    }
}
//--------------------------------------------------------------
void ofApp::newMidiMessage(ofxMidiMessage& msg) {
    // Store the incoming MIDI message
    midiMessage = msg;

    // Handle Control Change messages on Channel 2
    if (msg.status == MIDI_CONTROL_CHANGE && msg.channel == 2) {
        switch (msg.control) {
            case 77: // controls speed
                midiLoSpeed = ofMap(msg.value, 0, 127, 0.1, 1);
                break;
                
            case 78: // controls speed
                midiHiSpeed = ofMap(msg.value, 0, 127, 1, 90);
                break;

            case 79: // controls noise frequency
                midiLoNoise = ofMap(msg.value, 0, 127, 0.1, 3);
                break;
            case 80: // controls noise frequency
                midiHiNoise = ofMap(msg.value, 0, 127, 3, 3000);
                break;
            case 81: // controls noise frequency
                midiLoFreq = ofMap(msg.value, 0, 127, 9, 700);
                break;
            case 82: // controls noise frequency
                midiHiFreq = ofMap(msg.value, 0, 127, 0, 2048);
                break;


            case 83: // controls noise amplitude
                midiAmp = ofMap(msg.value, 0, 127, 0, 3055);
                break;

            case 20: // Strobe Speed Potentiometer (CC 20)
                strobeSpeed = ofMap(msg.value, 0, 127, 500, 50); // Adjust range as needed
                break;

            // Existing Strobe Pad (CC 60)
            case 104: // White Strobe Pad
                if (msg.value == 127) {
                    startStrobe(2); // Start the white strobe (now the 4th color in your array)
                } else if (msg.value == 0) {
                    stopStrobe(); // Stop any strobe
                }
                break;

            // New Strobe Pads for different colors
            case 105: // Red Strobe Pad
                if (msg.value == 127) { startStrobe(0); }
                else if (msg.value == 0) { stopStrobe(); }
                break;


            case 106: // Blue Strobe Pad
                if (msg.value == 127) { startStrobe(2); }
                else if (msg.value == 0) { stopStrobe(); }
                break;

            case 107: // Alternating Red/Blue Strobe Pad
                if (msg.value == 127) { startAltStrobe(); }
                else if (msg.value == 0) { stopAltStrobe(); }
                break;

            // Preset Pads
            case 73: // Preset 1 Pad
            case 74: // Preset 2 Pad
            case 75: // Preset 3 Pad
                handlePresetPad(msg.control);
                break;
        }
    }

    // Handle other types of MIDI messages if needed...
}
//--------------------------------------------------------------
// Strobe control functions
void ofApp::startStrobe(int colorIndex) {
    strobeState = true;
    currentStrobeColor = colorIndex;
    // Reset the background to black to start the strobe effect
    backgroundColor = ofColor(0, 0, 0);
}

void ofApp::stopStrobe() {
    strobeState = false;
    // Set the background back to black when the strobe is stopped
    backgroundColor = ofColor(0, 0, 0);
}

void ofApp::startAltStrobe() {
    altStrobeState = true;
    altStrobeIndex = 0; // Start with the first color in the sequence
    // Reset the background to black to start the alternating effect
    backgroundColor = ofColor(0, 0, 0);
}

void ofApp::stopAltStrobe() {
    altStrobeState = false;
    // Set the background back to black when the alternating strobe is stopped
    backgroundColor = ofColor(0, 0, 0);
}



// savePreset function: Responsible for saving the current color state as a preset.
void ofApp::savePreset(const std::string& presetName){
    ofJson preset; // Create an ofJson object to store the RGB values.
    preset["low_speed"] = midiLoSpeed; // Assign the current redValue to the 'r' key in the JSON object.
    preset["high_speed"] = midiHiSpeed; // Assign the current redValue to the 'r' key in the JSON object.

    preset["low_noise"] = midiLoNoise; // Assign the current greenValue to the 'g' key.
    preset["high_noise"] = midiHiNoise; // Assign the current greenValue to the 'g' key.
    
    preset["low_frequency"] = midiLoNoise; // Assign the current greenValue to the 'g' key.
    preset["high_frequency"] = midiHiNoise; // Assign the current greenValue to the 'g' key.
    
    

    preset["amplitude"] = midiAmp; // Assign the current blueValue to the 'b' key.
    


    // Save the JSON object to a file named after the presetName in the 'Presets' directory.
    ofSaveJson("Presets/" + presetName + ".json", preset);
    // This creates or overwrites a JSON file with the RGB values, effectively saving the preset.

    presetSaved[presetName] = true; // Mark the preset as saved in the presetSaved map.
    // This flag is used to determine whether a preset should be loaded or saved when a corresponding MIDI pad is pressed.
}

//--------------------------------------------------------------









// loadPreset function: Responsible for loading the saved color state from a preset.
void ofApp::loadPreset(const std::string& presetName){
    // Load the JSON file corresponding to the given presetName from the 'Presets' directory.
    ofJson preset = ofLoadJson("Presets/" + presetName + ".json");

    // Check if the JSON object is not empty, indicating a successful load.
    if (!preset.empty()) {
        // If the file is successfully loaded, extract the RGB values.
        midiLoSpeed = preset["low_speed"];
        midiHiSpeed = preset["high_speed"];

        midiLoNoise = preset["low_noise"];
        midiHiNoise = preset["high_noise"];
        
        midiLoFreq = preset["low_frequency"];
        midiHiFreq = preset["high_frequency"];

        midiAmp = preset["amplitude"];
        
        
        
        
        

        // These updated values will change the color of the triangle drawn in the draw() function.
    }
    // If the JSON object is empty, it implies the file did not exist or was empty. In this case, no action is taken.
}
//--------------------------------------------------------------

// exit function: Called when the application is about to close.
// This function is crucial for ensuring that resources are properly released and cleaned up.
void ofApp::exit() {
    // Close the MIDI port.
    // This is important to release the MIDI port so that other applications can use it.
    // Not closing the MIDI port might lead to it being locked or inaccessible in subsequent runs or other applications.
    midiIn.closePort();

    // Remove this instance of ofApp as a listener of the MIDI input.
    // This step is necessary to prevent the MIDI system from trying to send messages to this object
    // after it has been destroyed, which could cause crashes or undefined behaviors.
    midiIn.removeListener(this);

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseScrolled(int x, int y, float scrollX, float scrollY){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
