//
//  Particles.cpp
//  particles
//
//  Created by Raul on 11/28/23.
//

#include "Particles.h"
#include "ofApp.h"

const std::deque<ofVec2f>& Particle::getTrailPositions() const {
    return positions;
}





Particle::Particle(ofVec2f position) : position(position) {
    positions.push_back(position);
}

float Particle::getSize() const {
    return size;
}


void Particle::update(const std::vector<ofVec2f>& attractors, float noiseMultiplier) {
    // If in bounce mode, reduce its duration
    if (bounceModeDuration > 0) {
        bounceModeDuration--;
    } else {
        // Calculate the new velocity based on Perlin noise
        float noise = ofNoise(position.x * 0.01, position.y * 0.01) * TWO_PI * noiseMultiplier;
        ofVec2f noiseForce(cos(noise), sin(noise));
        velocity += noiseForce;
        velocity.limit(1); // Limiting the speed

        // Adding attractor forces
        for (auto& attractor : attractors) {
            ofVec2f dir = attractor - position;
            float distance = dir.length();
            if (distance < 1000 && distance > 0.001) {
                dir.normalize();
                ofVec2f perpendicular(-dir.y, dir.x);
                velocity += perpendicular / (distance / 200);
            }
        }
    }

    // Update position
    position += velocity * speed;

    // Edge collision detection
    if (position.x <= 0 || position.x >= ofGetWidth() || position.y <= 0 || position.y >= ofGetHeight()) {
        // Set bounce mode
        bounceModeDuration = 20;

        // Adjust the direction based on the edge hit
        if (position.x <= 0 || position.x >= ofGetWidth()) {
            velocity.x *= -.1; // Reverse X direction
            velocity.y += ofRandom(-5.9, 5.9); // Slight random adjustment to Y
        }
        if (position.y <= 0 || position.y >= ofGetHeight()) {
            velocity.y *= -.1; // Reverse Y direction
            velocity.x += ofRandom(-5.9, 5.9); // Slight random adjustment to X
        }

        position.x = ofClamp(position.x, 0, ofGetWidth());
        position.y = ofClamp(position.y, 0, ofGetHeight());
    }

    // Update trail
    positions.push_front(position);
    if (positions.size() > trailLength) {
        positions.pop_back();
    }
}



void Particle::draw() {
    ofSetColor(255);
    ofDrawCircle(position, size);
    
    int index = 0;
    float alphaStep = 255.0f / trailLength;
    for (auto& pos : positions) {
        ofSetColor(ofRandom(255), 55, 255, 155 - (index * alphaStep));
        ofDrawCircle(pos, size * (1.0 - (index / (float)trailLength))); // For a decreasing trail size
        // ofDrawCircle(pos, size); // For a fixed size trail
        index++;
    }
}

ofVec2f Particle::getPosition() const {
    return position;
}
