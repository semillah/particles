#pragma once

#include "ofMain.h"
#include "Particles.h" // Include the new Particle header
#include "ofSoundPlayer.h"
#include "ofxMidi.h" // Include the MIDI addon header for MIDI functionality
#include "ofJson.h" // Include JSON support for handling preset data



class ofApp : public ofBaseApp, public ofxMidiListener{

public:
    void setup();
    void update();
    void draw();
    
    void exit();
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseScrolled(int x, int y, float scrollX, float scrollY);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void gotMessage(ofMessage msg);
    void dragEvent(ofDragInfo dragInfo);
    float lowFreqAmplitude;
    ofSoundPlayer soundPlayer; // Sound player to play the music
        float *fftSmoothed;       // Array to store FFT analysis results
        int nBandsToGet;          // Number of bands to analyze
    
    // MIDI message handling
    void newMidiMessage(ofxMidiMessage& eventArgs);
    // newMidiMessage(): Callback triggered when a MIDI message is received.
    // It's used to process MIDI input like knob movements or pad presses.

    // MIDI Input management
    ofxMidiIn midiIn; // midiIn: Object for handling incoming MIDI messages.
    ofxMidiMessage midiMessage; // midiMessage: Variable to store the last received MIDI message.
    // The midiIn object listens to a MIDI port and forwards messages to newMidiMessage().
    
    
    
    
    
    
    
    // variables to map to midi
    int midiLoSpeed; //
    int midiHiSpeed; //

    int midiLoNoise; //
    int midiHiNoise; //

    int midiAmp; //
    
    int midiLoFreq; //
    int midiHiFreq; //

    
    
    
    
    
    
    
    
    
    // Strobe Effect Enhancements
    bool strobeState; // Tracks whether any strobe effect is on or off
    int strobeSpeed; // Controls the speed of the strobe effect
    float lastStrobeTime; // Tracks the time since the last strobe change
    ofColor backgroundColor; // Current background color
    
    
    
    

    // New Variables for Multi-Colored Strobe
    ofColor strobeColors[3]; // Array to hold the different single-color strobe colors (Red, Green, Blue)
    int currentStrobeColor; // Index of the current strobe color
    bool multiStrobeState; // Tracks whether the multi-color strobe effect is on or off
    
    

    // Variables for Alternating Red/Blue Strobe
    bool altStrobeState; // Tracks whether the alternating Red/Blue strobe effect is on or off
    int altStrobeIndex; // Index to track the current color in the alternating sequence (0 for red, 1 for black, 2 for blue)
    float altStrobeLastTime; // Tracks the time since the last color change in the alternating sequence

    
    

    // New methods for handling the strobe effects
    void startStrobe(int colorIndex); // Start single-color strobe with specified color
    void stopStrobe(); // Stop any strobe effect
    void startAltStrobe(); // Start alternating Red/Blue strobe
    void stopAltStrobe(); // Stop alternating Red/Blue strobe
    void updateAltStrobe(); // Update the alternating Red/Blue strobe effect
    
    
    
    
    
    
    
    

        // Preset management
        std::map<std::string, ofJson> presets; // presets: Map linking preset names to their JSON data.
        std::map<std::string, bool> presetSaved; // presetSaved: Map tracking if a preset has been saved.
        // The presets map stores the color configuration of each preset, while presetSaved indicates
        // whether a specific preset has been saved to prevent overwriting existing presets.

        // Preset functions
        void savePreset(const std::string& presetName); // savePreset(): Saves the current RGB values as a preset.
        void loadPreset(const std::string& presetName); // loadPreset(): Loads RGB values from a saved preset.
        void handlePresetPad(int padControl); // handlePresetPad(): Decides to save or load a preset based on pad input.
        // This function is central to preset management, handling the logic of saving new presets
        // or loading existing ones when MIDI pads are interacted with.

    

private:
    static constexpr int trailLength = 15; // Centralized trail length definition
    std::vector<Particle> particles;
    std::vector<ofVec2f> attractors;
    ofVbo particleVbo; // Vertex Buffer Object for particles
    std::vector<ofVec3f> particlePositions; // Positions for each particle
    std::vector<ofFloatColor> particleColors; // Colors for each particle (if needed)
    int frameCount = 0;
    const int maxFrame = 4200;
};
