//
//  Particles.h
//  particles
//
//  Created by Raul on 11/28/23.
//

#ifndef Particles_h
#define Particles_h

#include "ofMain.h"
#include <deque>

class Particle {
public:
    Particle(ofVec2f position);
    float getSize() const;

    void update(const std::vector<ofVec2f>& attractors);
    void draw();
    const std::deque<ofVec2f>& getTrailPositions() const;

    ofVec2f getPosition() const; // Method to get the particle's position for VBO

    void setSize(float s) { size = s; }
    void setTrailLength(int len) { trailLength = len; }
    void setSpeed(float s) { speed = s; }
    void update(const std::vector<ofVec2f>& attractors, float noiseMultiplier); // Updated method signature
    int midiAmp; //




private:
    int bounceModeDuration = 0; // Duration for which the bounce override is active
    ofVec2f position, velocity;
    float speed = .1f;
    float size = 1.0f;
    int trailLength = 15;
    std::deque<ofVec2f> positions; // Store past positions for the trail
};

#endif /* Particles_h */
